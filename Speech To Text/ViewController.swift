//
//  ViewController.swift
//  Speech To Text
//
//  Created by Adrian Young-Hoon on 2017-09-10.
//  Copyright © 2017 Rapid MAP Media. All rights reserved.
//

import UIKit
import Speech

public class ViewController: UIViewController, SFSpeechRecognizerDelegate {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var buttonTranscribe: UIButton!
    
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "en-US"))!
    private let audioEngine = AVAudioEngine()
    
    private var speechRecognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    private var speechRecognitionTask: SFSpeechRecognitionTask?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        buttonTranscribe.isEnabled = false
    }

    override public func viewDidAppear(_ animated: Bool) {
        speechRecognizer.delegate = self
        
        SFSpeechRecognizer.requestAuthorization {authorizationStatus in
            OperationQueue.main.addOperation {
                switch authorizationStatus {
                case .authorized:
                    self.buttonTranscribe.isEnabled = true
                case .denied:
                    self.buttonTranscribe.isEnabled = false
                    self.buttonTranscribe.setTitle("User denied access to speech recognition.", for: .disabled)
                case .restricted:
                    self.buttonTranscribe.isEnabled = false
                    self.buttonTranscribe.setTitle("Speech recognition restricted on device.", for: .disabled)
                case .notDetermined:
                    self.buttonTranscribe.isEnabled = false
                    self.buttonTranscribe.setTitle("Speech recognition not yet authorized.", for: .disabled)
                }
            }
        }
    }
    
    private func StartRecording() throws {
        if let recognitionTask = speechRecognitionTask {
            recognitionTask.cancel()
            self.speechRecognitionTask = nil
        }
        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(AVAudioSessionCategoryRecord)
        try audioSession.setMode(AVAudioSessionModeMeasurement)
        try audioSession.setActive(true, with: .notifyOthersOnDeactivation)
        
        speechRecognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        guard let inputNode = audioEngine.inputNode else {fatalError("Audio engin has no input node.")}
        guard let recognitionRequest = speechRecognitionRequest else {fatalError("Unable to create a SFSpeechAudioBufferRecognitionRequest object.")}
        
        speechRecognitionRequest?.shouldReportPartialResults = true
        
        speechRecognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) {result, error in var isFinal = false
            if let result = result {
                self.textView.text = result.bestTranscription.formattedString
                isFinal = result.isFinal
            }
            if error != nil || isFinal {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.speechRecognitionRequest = nil
                self.speechRecognitionTask = nil
                self.buttonTranscribe.isEnabled = true
                self.buttonTranscribe.setTitle("Start Speaking...", for: [])
            }
        }
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.speechRecognitionRequest?.append(buffer)
        }
        
        audioEngine.prepare()
        try audioEngine.start()
        
        textView.text = "Start talking I'm listening..."
        
    }
    
    public func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            buttonTranscribe.isEnabled = true
            buttonTranscribe.setTitle("Start Speaking...", for: [])
        }
        else {
            buttonTranscribe.isEnabled = false
            buttonTranscribe.setTitle("Speech recognition not available.", for: .disabled)
        }
    }
    
    @IBAction func buttonActionTranscribe(_ sender: UIButton) {
        if audioEngine.isRunning {
            audioEngine.stop()
            speechRecognitionRequest?.endAudio()
            buttonTranscribe.isEnabled = false
            buttonTranscribe.setTitle("Ending...", for: .disabled)
        }
        else {
            try! StartRecording()
            buttonTranscribe.setTitle("Stop Recording", for: [])
        }
    }
    
}

